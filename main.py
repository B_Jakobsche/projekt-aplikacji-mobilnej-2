import kivy
from kivy.properties import StringProperty, ObjectProperty
from kivy.uix.floatlayout import FloatLayout

from kivymd.app import MDApp
from kivymd.toast import toast
from kivymd.uix.boxlayout import MDBoxLayout
from kivy.uix.scrollview import ScrollView
from kivy.metrics import dp
from kivymd.uix.button import MDFlatButton, MDIconButton
from kivymd.uix.chip import MDChip
from kivymd.uix.dialog import MDDialog
from kivy.uix.popup import Popup
from kivymd.uix.expansionpanel import MDExpansionPanel, MDExpansionPanelThreeLine
from kivymd.uix.label import MDLabel
from kivymd.uix.list import TwoLineListItem, OneLineListItem, IRightBodyTouch, OneLineAvatarIconListItem, MDList, \
    ThreeLineListItem
from kivymd.uix.menu import MDDropdownMenu
from kivymd.uix.snackbar import Snackbar
import sqlite3

fridge = ""  # lista składników
recipe = set()


class Container(IRightBodyTouch, MDBoxLayout):
    adaptive_width = True


class Content(MDBoxLayout):
    pass


class ContentR(MDBoxLayout):
    pass


class Kontent(MDBoxLayout):
    pass


class PotList(MDBoxLayout):
    pass

class SubtitlesChip(MDChip):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.is_checked = False

    def on_touch_down(self, touch):
        super(SubtitlesChip, self).on_touch_down(touch)
        self.is_checked = False
        if self.ids.box_check.children:
            self.is_checked = True

class MyApp(MDApp):
    dialog = None
    dialog1 = None
    dialog2 = None
    dialog3 = None
    dialog4 = None



    def build(self):
        # self.root.ids.menu.children[3].text = l
        pass

    def build_list(self, fridge: str):
        self.root.ids.container.clear_widgets()
        result = self._match_recipes(fridge)
        print(result)
        scroll = ScrollView()

        list_view = MDList()
        for k in result:
            ing = ""
            for wo in result[k][0]:
                ing += wo
                ing += "   "

            items = ThreeLineListItem(text=str(k),
                                      secondary_text=ing,
                                      tertiary_text=str(result[k][2]),
                                      on_release=self._event_hendle(result[k][1]))

            list_view.add_widget(items)

        scroll.add_widget(list_view)
        # End List
        self.root.ids.container.add_widget(scroll)

    def build_list_drink(self, fridge: str):

        self.root.ids.container_drink.clear_widgets()
        result = self._match_recipes_drink(fridge)

        scroll = ScrollView()

        list_view = MDList()
        for k in result:
            ing = ""
            for wo in result[k][0]:
                ing += wo
                ing += "   "

            items = ThreeLineListItem(text=str(k),
                                      secondary_text=ing,
                                      tertiary_text=str(result[k][2]),
                                      on_release=self._event_hendle(result[k][1]))

            list_view.add_widget(items)

        scroll.add_widget(list_view)
        # End List
        self.root.ids.container_drink.add_widget(scroll)

    def _event_hendle(self, list_object):
        def f(self):
            self.dialog3 = MDDialog(

                # content_cls=Show_rec(),
                text=list_object

            )
            self.dialog3.width=280
            return self.dialog3.open()

        return f

    def login_screen(self):
        show = Content()
        self.popupWindowLogin = Popup(
            title=" ", content=show, size_hint=(None, None), size=(300, 300)
        )
        self.popupWindowLogin.open()
        # if not self.dialog:
        #     self.dialog = MDDialog(
        #
        #         type="custom",
        #         content_cls=Content()
        #     )
        #
        #     self.dialog.open()

    def logger(self):
        l = "Użytkownik: "
        self.USER_SESSION=""
        login = self.popupWindowLogin.content.children[2].text
        password = self.popupWindowLogin.content.children[1].text
        print(login, password)
        cursor = self.con.cursor()
        temp = cursor.execute("SELECT name, password,preferences  FROM Persons").fetchall()
        for t in temp:


            print(login,t[0],t[1],password)
            # print(*temp)
            if login == t[0] and password == t[1]:
                self.USER_SESSION = [login,t[2]]
                l +=login
                break
        if not self.USER_SESSION :

            toast("błędne hasło lub login" )

        self.root.ids.menu.children[3].text = l

        self.popupWindowLogin.dismiss()

    def register(self):
        handicap = ""
        login = self.popupWindowRegister.content.children[4].text
        password = self.popupWindowRegister.content.children[3].text
        passwordS = self.popupWindowRegister.content.children[2].text
        if self.popupWindowRegister.content.children[1].children[0].children[2].is_checked:
            handicap += "v,"
        if self.popupWindowRegister.content.children[1].children[0].children[1].is_checked:
            handicap += "e,"
        if self.popupWindowRegister.content.children[1].children[0].children[0].is_checked:
            handicap += "gl"
        print(login, password, passwordS, handicap)
        assert password == passwordS
        assert login != "" and password != "" and login != " " and password != " "
        cursor = self.con.cursor()
        print(f"""INSERT INTO Persons (name, password, preferences) VALUES('{login}', "{password}", "{handicap}");""")
        cursor.execute(
            f"""INSERT INTO Persons (name, password, preferences) VALUES('{login}', "{password}", "{handicap}");"""
        )
        self.con.commit()
        self.popupWindowRegister.dismiss()

    def register_screen(self):
        show = Kontent()
        self.popupWindowRegister = Popup(
            title=" ", content=show, size_hint=(None, None), size=(300, 400)
        )
        self.popupWindowRegister.open()
        # if not self.dialog1:
        #     self.dialog1 = MDDialog(
        #
        #         type="custom",
        #         content_cls=Kontent()
        #     )
        #     self.dialog1.open()

    def add_recipe(self):
        self.show = ContentR()

        self.popupWindow = Popup(
            title=" ", content=self.show, size_hint=(None, None), size=(300, 400)
        )

        self.popupWindow.open()

        # if not self.dialog4:
        #     self.dialog4 = MDDialog(
        #
        #         type="custom",
        #         content_cls=ContentR()
        #
        #     )
        #     self.dialog4.open()

    def submit(self):
        fridge = self.root.ids.fr.text
        print(fridge)
        self.build_list(fridge)
        self.build_list_drink(fridge)

    def on_start(self):
        # self.root.ids.menu.children[3].text =  l
        self.con = sqlite3.connect("sqlite_2.db")
        # self.recipe_added()
        # self.build_list('mięta,limąka,cukier')
        # self.build_list(fridge)
        # self.match_Recipes()

    def _match_recipes(self, fridge: str):
        fridge = set([el.strip() for el in fridge.split(",")])
        kursor = self.con.cursor()
        temp = kursor.execute("select id,ingredients from recipes ").fetchall()

        s = {tup[0]: set([el.strip() for el in tup[1].split(',')]) for tup in temp}
        matched = []
        for k, v in s.items():

            if v.difference(fridge) == set():
                matched.append(k)
        matched = tuple(matched)

        member = ""
        try:
            if self.USER_SESSION:
                keys = self.USER_SESSION[1].split(",")
                if "v" in keys:
                    member = 'and membership like "%v%"'
                elif "e" in keys:
                    member = 'and (membership like "%v%" or membership like  "%e%") '

                if "gl" in keys:
                    member += 'and membership like "%gl%"'
        except:
            member = ""
        try:
            out = kursor.execute(
                f'select * from recipes where id in {matched} and (membership not like "%D%" )  {member} ;').fetchall()
        except:
            out = kursor.execute(
                f'select * from recipes where id in ({matched[0]}) and (membership not like "%D%" )  {member} ;').fetchall()

        print(f'select * from recipes where id in {matched} and membership not like "%D,%" {member} ;', out)
        print({item[1]: ([ing + " " + q for ing, q in zip(item[2].split(","), item[3].split(","))], item[4], item[5])
               for item in out})
        return {item[1]: ([ing + " " + q for ing, q in zip(item[2].split(","), item[3].split(","))], item[4], item[5])
                for item in out}

    def _match_recipes_drink(self, fridge: str):
        fridge = set([el.strip() for el in fridge.split(",")])
        kursor = self.con.cursor()
        temp = kursor.execute("select id,ingredients from recipes ").fetchall()

        s = {tup[0]: set(tup[1].split(',')) for tup in temp}
        matched = []
        for k, v in s.items():

            if v.difference(fridge) == set():
                matched.append(k)
        matched = tuple(matched)

        try:
            out = kursor.execute(f'select * from recipes where id in {matched} and membership = "D"').fetchall()
        except:
            out = kursor.execute(f'select * from recipes where id in ({matched[0]}) and membership = "D"').fetchall()

        return {item[1]: ([ing + " " + q for ing, q in zip(item[2].split(","), item[3].split(","))], item[4], item[5])
                for item in out}


    def recipe_added(self):
        drop_item = ""
        try:
            name = self.popupWindow.content.children[1].children[2].text
            ingredients = self.popupWindow.content.children[1].children[1].text
            description = self.popupWindow.content.children[1].children[0].text

            dr = self.popupWindow.content.children[0].children[1].text
            if dr == "vege":
                drop_item += "v,"
            if dr == "vegetariańskie":
                drop_item += "e,"
            if dr == "gluten-free":
                drop_item += "gl,"
            if dr == "drink":
                drop_item += "D,"
            if dr == "typ":
                drop_item += ""

            recipe = (name, ingredients, description)
            ingqu = ingredients.split(",")
            ing = []
            qu = []
            for i in ingqu:
                s = i.split(" ")
                # s.remove("")

                ing.append(s[0])
                qu.append(s[1])

            print(ing, qu)

            cursor = self.con.cursor()
            cursor.execute(
                f"""INSERT INTO recipes ("name","ingredients","quantity","description","membership") Values
                ("{name}","{",".join(ing)}","{",".join(qu)}","{description}","{drop_item}");"""
            )
            self.con.commit()
            self.popupWindow.dismiss()
        except:
            toast('Przepis został błędnie wprowadzony ')


if __name__ == '__main__':
    MyApp().run()

#
